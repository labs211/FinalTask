import os
from datetime import datetime
from flask import Flask, request, jsonify, make_response
from flask_cors import CORS, cross_origin
from flask_pymongo import PyMongo

application = Flask(__name__)
cors = CORS(application)
application.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

mongo = PyMongo(application)
db = mongo.db


@application.route('/api')
@cross_origin()
def index():
    _slides = db.slides.find()
    item = {}
    data = []
    for slide in _slides:
        item = {
            '_id': str(slide['_id']),
            'thumbnailPath': slide['thumbnailPath'],
            'videoUrl': slide['videoUrl'],
        }
        data.append(item)
    resp = make_response(jsonify(
        status=True,
        data=data
    ))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@application.route('/add', methods=['POST'])
@cross_origin()
def add():
    data = request.get_json(force=True)
    item = {
        'thumbnailPath': data['thumbnailPath'],
        'videoUrl': data['videoUrl']
    }
    db.slides.insert_one(item)

    return jsonify(
        status=True,
        message='To-do saved successfully!'
    ), 201

if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    application.run(host='0.0.0.0', port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)